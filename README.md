**Raleigh neurologist**

Our finest Raleigh NC neurologists are highly qualified and specially trained, assisted by experience and supported by state-of-the-art services, 
to diagnose and treat a wide range of neurological disorders.
While our broad size gives us the opportunity to provide highly trained specialists in many distinct neurological areas, 
we have never wavered from our mission: patient-focused, quality-driven care. 
We maintain the same personal relationships that our practice has built between patient and physician.
Please Visit Our Website [Raleigh neurologist](https://neurologistraleighnc.com/) for more information. 

---

## Our neurologist in Raleigh team

We will demonstrate compassion and provide exceptional healthcare to improve the lives of our patients and their families. 
We have four dedicated pediatric neurologists, a development pediatrician, 
and three pediatric nurse practitioners, making us one of the largest child neurology practices in the country outside a university setting.
Raleigh Neurology expanded our team of physician assistants and nurse practitioners in 2012 to support the physicians in our private offices, 
while closely monitoring healthcare trends.


